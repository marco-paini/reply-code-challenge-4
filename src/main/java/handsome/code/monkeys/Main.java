package handsome.code.monkeys;

import java.net.URL;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class Main implements QuarkusApplication {
	@ConfigProperty(name = "limit", defaultValue = "4")
	long limit;

	@ConfigProperty(name = "skip", defaultValue = "0")
	long skip;

	private String input(final String string) {
		return "/input/" + string;
	}

	@Override
	public int run(String... args) throws Exception {
		Stream.of("first_adventure.in", "second_adventure.in", "third_adventure.in", "fourth_adventure.in").skip(skip)
				.limit(limit).map(this::input).map(getClass().getClassLoader()::getResource).map(URL::getPath)
				.map(Paths::get).forEach(HandsomeCodeMonkeys::new);
		return 0;
	}
}
