package handsome.code.monkeys.data;

import java.util.ArrayList;

import lombok.Data;
import lombok.NonNull;

@Data
public class Project {
	@NonNull
	public String name;

	@NonNull
	public Integer penality;

	public ArrayList<Integer> units = new ArrayList<>();
}
