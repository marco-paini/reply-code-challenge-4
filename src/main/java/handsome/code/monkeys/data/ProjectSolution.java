package handsome.code.monkeys.data;

import lombok.Data;

@Data
public class ProjectSolution {
	public int nPackage;

	public int providerIndex;

	public float rank;

	public int regionIndex;

	public ProjectSolution(int providerIndex, int regionIndex, int nPackage) {
		this.providerIndex = providerIndex;
		this.regionIndex = regionIndex;
		this.nPackage = nPackage;
	}
}
